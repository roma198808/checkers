class Piece

  SLIDE = [
    [-1,+1],
    [+1,+1],
    [+1,-1],
    [-1,-1]
  ]

  JUMP = [
    [-2,+2],
    [+2,+2],
    [+2,-2],
    [-2,-2]
  ]

  attr_reader :color, :icon

  def initialize(color)
    @king  = false
    @color = color
    @color == "white" ? (@icon = "\u25CB") : (@icon = "\u25CF")
  end

  def perform_moves!(from, to, board_o)
    raise "Invalid Move" unless valid_move_seq?(from, to, board_o)

    from = from
    to.each do |move|
      capture_when_jump(from, move, board_o)
      maybe_promote(move)

      board_o.rows[move] = self
      board_o.rows[from] = nil

      from = move
    end

    nil
  end



  private

  # generates an array of a difference between the moves
  # used to check with contants
  def move_diff(from, to)
    from = convert_to_array(from) unless from.is_a?(Array)
    to   = convert_to_array(to)   unless to.is_a?(Array)

    [to, from].transpose.map { |each| each.inject(:-) }
  end

  # will convert b3 => [1,3]
  def convert_to_array(input)
    [ input[0].ord-97 , input[1..2].to_i ]
  end

  # will convert [1,3] => b3
  def convert_to_hash_key(input)
    "#{(input[0]+97).chr}#{input[1].to_s}"
  end

  # will remove a jumped piece from the board
  def capture_when_jump(from, to, board_o)
    move_diff = move_diff(from, to)
    return nil unless JUMP.include?(move_diff)

    between = move_diff.map { |el| el / 2 }
    between = convert_to_hash_key(
               [ convert_to_array(from) , between ].
               transpose.map { |each| each.inject(:+) } )

    if board_o.rows[between].nil? ||
       board_o.rows[between].color == board_o.rows[from].color
       raise "Invalid Jump"
    end

    board_o.rows[between] = nil
  end

  # checks if the moves can be performed based on cell change
  def valid_move_seq?(from, to, board_o)
    from = from

    to.each do |pos_move|
      unless board_o.rows.include?(pos_move)
        raise "Move is outside the board"
      end

      unless board_o.rows[pos_move].nil?
        raise "Can't step on top of another checker"
      end

      move_diff = move_diff(from, pos_move)
      slide, jump = get_slides[0], get_slides[1]

      if slide.include?(move_diff)
        to.count == 1 ? (return true) : (return false)
      end

      return false unless jump.include?(move_diff)
      from = pos_move
    end

    true
  end

  # returns proper possible moves for checkers depending on
  # the color and whether it is a king or not
  def get_slides
    slide = nil
    jump  = nil

    if @color == "white"
      slide = SLIDE[0..1]
      jump  = JUMP[0..1]
    end

    if @color == "black"
      slide = SLIDE[2..3]
      jump  = JUMP[2..3]
    end

    slide = SLIDE if @king
    jump  = JUMP  if @king

    [ slide , jump ]
  end

  # promotes to king if possible
  def maybe_promote(move)
    @king = true if move[1..2].to_i == 10 && @color == "white"
    @king = true if move[1..2].to_i == 1  && @color == "black"
    @icon = "\u2654" if @king && @color == "white"
    @icon = "\u265A" if @king && @color == "black"
  end

end
